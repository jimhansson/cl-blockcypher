;;;; package.lisp

(defpackage #:cl-blockcypher
	(:documentation "A wrapper around the blockcypher API. It's not done and we will add
	more to it as we need. All the slots that are optional in blockcyhper's API spec may be
	unbound in object returned from this API wrapper to signal that it was not present in
	the data returned from blockcyhper.")
  (:use #:cl
				#:drakma
				#:yason
				#:json-mop)
	(:export
	 ;; parameters.
	 #:+base-uri+ 
	 #:+token+
	 #:+coin+
	 #:+chain+
	 ;; classes
	 #:<blockchain> 
	 #:<block>
	 #:<tx-input>
	 #:<tx-output>
	 #:<tx-ref>
	 #:<tx>
	 #:<addres>
	 #:<hd-address>
	 #:<hd-chain>
	 #:<hd-wallet>
	 ;; functions
	 #:blockchain
	 #:wallets
	 #:wallet
	 #:create-wallet
	 #:derive-address
	 #:delete-wallet))
