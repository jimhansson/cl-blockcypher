;;;; cl-blockcypher.lisp

(in-package #:cl-blockcypher)

(defconstant +base-uri+ "https://api.blockcypher.com/v1/"
	"the base uri for accessing the blockcypher API, you normally don't want to change this. but if it is a versions 2 that are somewhat compatiable then you might wan to change this")
(defparameter +token+ "70cf719045c44dcd95e7e7dfe0c93b33"
	"Set this to your own token, that you have been given when registring with blockcypher.")
(defparameter +coin+ "bcy"
	"The coin you want to query about.")
(defparameter +chain+ "test"
	"test or main")

;; debugging aid.
(setf drakma:*header-stream* *standard-output*)

(defun request (uri &key (method :get) data)
	"do request and return the stream"
	(let ((url (concatenate 'string +base-uri+ +coin+ "/" +chain+ "/" uri "?token=" +token+)))
		(drakma:http-request url :method method :want-stream t :content data)))

;; we are using json-mop for both encoding and decoding should avoid using yason directly.

(defclass <blockchain> ()
	((name :json-type :string :json-key "name")
	 (height :json-type :number :json-key "height")
	 (hash :json-type :string :json-key "hash")
	 (time :json-key "time")
	 (latest-url :json-key "latest_url")
	 (previous-hash :json-key "previous_hash")
	 (previous-url :json-key "previous_url")
	 (peer-count :json-type :number :json-key "peer_count")
	 (high-fee-per-kb :json-key "high_fee_per_kb")
	 (medium-fee-per-kb :json-key "medium_fee_per_kb")
	 (low-fee-per-kb :json-key "low_fee_per_kb")
	 (unconfirmed-count :json-key "unconfirmed_count")
	 (last-fork-height :json-key "last_fork_height")
	 (last-fork-hash :json-key "last_fork_hash"))
	(:metaclass json-serializable-class)
	(:documentation "Represent the state of the blockchain"))

(defclass <block> ()
	((hash :json-key "hash")
	 (height :json-key "height" :json-type :number)
	 (depth :json-key "depth" :json-type :number)
	 (chain :json-key "chain")
	 (total :json-key "total" :json-type :number)
	 (fees :json-key "fees" :json-type :number)
	 (size :json-key "size" :json-type :number)
	 (ver :json-key "ver" :json-type :number)
	 (received-time :json-key "received_time")
	 (relayed-by :json-key "relayed_by")
	 (bits :json-key "bits")
	 (nonce :json-key "nonce")
	 (n-tx :json-key "n_tx")
	 (prev-block :json-key "prev_block")
	 (prev-block-url :json-key "prev_block_url")
	 (tx-url :json-key "tx_url")
	 (mrkl-root :json-key "mrkl-root")
	 (txids :json-key "txids" :json-type (:vector :string))
	 (next-txids :json-key "next_txids")) 
	(:metaclass json-serializable-class))

(defclass <tx-input> ()
	((prev-hash :json-key "prev_hash")
	 (output-index :json-key "output_index")
	 (output-value :json-key "output_value")
	 (script-type :json-key "script_type")
	 (script :json-key "script")
	 (addresses :json-type (:vector :string) :json-key "addresses")
	 (sequence :json-key "sequence")
	 (age :json-key "age")
	 (wallet-name :json-key "wallet_name")
	 (wallet-token :json-key "wallet_token"))
	(:metaclass json-serializable-class))

(defclass <tx-output> ()
	((value :json-type :number :json-key "value")
	 (script :json-key "script")
	 (addresses :json-type (:vector :string) :json-key "addresses")
	 (script-type :json-key "script_type")
	 (spent-by :json-key "spent_by")
	 (data-hex :json-key "data_hex")
	 (data-string :json-key "data_string"))
	(:metaclass json-serializable-class))

(defclass <tx-ref> ()
	((address :json-key "address")
	 (block-height :json-type :number :json-key "block_height")
	 (tx-hash :json-key "tx_hash")
	 (tx-input-n :json-type :number :json-key "tx_input_n")
	 (tx-output-n :json-type :number :json-key "tx_output_n")
	 (value :json-type :number :json-key "value")
	 (preference :json-key "preference")
	 (spent :json-type :bool :json-key "spent")
	 (double-spend :json-type :bool :json-key "double_spent")
	 (confirmations :json-type :number :json-key "confirmations")
	 (script :json-key "script")
	 (ref-balance :json-type :number :json-key "ref_balance")
	 (confidence :json-key "confidence")
	 (confirmed :json-key "confirmed")
	 (spent-by :json-key "spent_by")
	 (received :json-key "received")
	 (receive-count :json-key "receive_count")
	 (double-of :json-key "double_of"))
	(:metaclass json-serializable-class))

(defclass <tx> ()
	((block-height :json-key "block_height")
	 (hash :json-key "hash")
	 (addresses :json-type (:vector <address>) :json-key "addresses")
	 (total :json-key "total")
	 (fees :json-key "fees")
	 (size :json-key "size")
	 (preference :json-key "preference")
	 (relayed-by :json-key "relayed-by")
	 (ver :json-type :integer :json-key "ver")
	 (lock-time :json-type :integer :json-key "lock_time")
	 (double-spend :json-type :bool :json-key "double_spend")
	 (vin :json-type :number :json-key "vin_sz")
	 (vout :json-type :number :json-key "vout_sz")
	 (confirmations :json-type :number :json-key "confirmations")
	 (inputs :json-type (:vector <tx-input>) :json-key "inputs")
	 (outputs :json-type (:vector <tx-output>) :json-key "outputs")
	 (opt-in-rbf :json-type :bool :json-key "opt_in_rbf")
	 (confidence :json-type :number :json-key "confidence")
	 (confirmed :json-key "confirmed")
	 (receive-count :json-type :number :json-key "receive_count")
	 (change-address :json-type :string :json-key "change_address")
	 (block-hash :json-key "block_hash")
	 (block-index :json-key "block_index")
	 (double-of :json-key "double_of")
	 (hex :json-key "hex")
	 (next-inputs :json-key "next_inputs")
	 (next-ouputs :json-key "next_outputs"))
	(:metaclass json-serializable-class))

(defclass <address> ()
	((address :json-key "address")
	 (wallet :json-key "wallet")
	 (hd-wallet :json-key "hd_wallet")
	 (total-received :json-type :number :json-key "total_received")
	 (total-sent :json-type :number :json-key "total_sent")
	 (balance :json-type :number :json-key "balance")
	 (unconfirmed-balance :json-type :number :json-key "unconfirmed_balance")
	 (final-balance :json-type :number :json-key "final_balance")
	 (n-tx :json-type :number :json-key "n_tx")
	 (unconfirmed-tx :json-type :number :json-key "unconfirmed_tx")
	 (final-n-tx :json-type :number :json-key "final_n_tx")
	 (tx-url :json-key "tx_url")
	 (txs :json-type (:vector <tx>) :json-key "txs")
	 (txrefs :json-type (:vector <txref>) :json-key "txrefs")
	 (unconfirmed-txrefs :json-type (:vector <txref>) :json-key "unconfirmed_txrefs")
	 (has-more :json-type :bool :json-key "hasMore"))
	(:metaclass json-serializable-class))

(defclass <hd-address> ()
	((address :json-key "address")
	 (path :json-key "path")
	 (public :json-key "public"))
	(:metaclass json-serializable-class))

(defclass <hd-chain> ()
	((chain-addresses :json-type (:vector <hd-address>) :json-key "chain_addresses")
	 (index :json-type :number :json-key "index"))
	(:metaclass json-serializable-class))

(defclass <hd-wallet> ()
	((token :json-key "token")
	 (name :json-key "name" :initarg :name :initform nil)
	 (chains :json-key "chains" :json-type (:vector <hd-chain>))
	 (hd :json-key "hd" :json-type :bool :initform t)
	 (extended-public-key :json-key "extended_public_key"
												:initarg :xpub :reader xpub)
	 (subchain-indexes :json-key "subchain_indexes"
										 :initarg :subchain-indexes))
	(:metaclass json-serializable-class))


(defclass <address~keychain> ()
	((address :json-key "address" :initarg :address :initform nil)
	 (public :json-key "public")
	 (private :json-key "private")
	 (wif :json-key "wif")
	 (pubkeys :json-key "pubkeys")
	 (script-type :json-key "script_type")
	 (original-address :json-key "original_address")
	 (oap-address :json-key "oap_address"))
	(:metaclass json-serializable-class))

(defun blockchain (&key (network :btcmain))
	(json-mop:json-to-clos (request "") '<blockchain>))

(defun wallets ()
	(let ((stream (request "wallets" :method :get)))
		(yason:parse stream :object-as :plist)))

(defun wallet (name)
	(let ((stream (request (concatenate 'string "wallets/hd/" name) :method :get)))
			(json-mop:json-to-clos stream '<hd-wallet>)))

(defun create-wallet (wallet)
	(check-type wallet <hd-wallet>)
	(assert (slot-value wallet 'hd))
	(assert (slot-value wallet 'name))
	(assert (slot-value wallet 'extended-public-key))
	(json-mop:json-to-clos (request "wallets/hd"
																	:method :post
																	:data (with-output-to-string (*standard-output*)
																					(yason:encode wallet)))
												 '<hd-wallet>))

(defun derive-address (wallet &optional (count 1) subchain-index)
	(check-type wallet <hd-wallet>)
	(let ((stream (request (concatenate 'string "wallets/hd/" (slot-value wallet 'name) "/addresses/derive") :method :post)))
		(json-mop:json-to-clos stream '<hd-wallet>)))


(defgeneric delete-wallet (wallet))

(defmethod delete-wallet ((wallet string))
	(request (concatenate 'string "wallets/hd/" wallet) :method :delete))

(defmethod delete-wallet ((wallet <hd-wallet>))
 (delete-wallet (slot-value wallet 'name)))


(defun address-balance (address-or-wallet &optional (omit-wallet-addresses t)))
(defun address (address-or-wallet &key unspent-only include-script include-confidence before after limit confirmations confidence omit-wallet-addresses))
(defun address-full (address-or-wallet &key before after limit txlimit confirmations confidence include-hex include-confidence omit-wallet-addresses))



;; how to convert a yason object ito 
;; we use the json-to-clos from json-mop, it is built ontop of yason but yason does not
;; allow you directly map decoded json into objects that is something that json-mop helps
;; with.

;; returns a addresskeychain object, 
(defun create-address ()
	(let ((req (yason:with-output-to-string* ()
							 (yason:with-object ()
								 (yason:encode-object-element "foobar" "foobar")))))
		(multiple-value-bind (body code headers uri stream close-stream reason)
				(request "addrs" :data req :method :post)
			(json-to-clos body '<address~keychain>))))

(defun faucet (address amount)
	(let ((req (yason:with-output-to-string* ()
							 (yason:with-object ()
								 (yason:encode-object-elements "address" address
																							 "amount" amount)))))
		(multiple-value-bind (body code headers uri stream close-stream reason)
				(request "faucet" :data req))))
