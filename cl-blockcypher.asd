;;;; cl-blockcypher.asd

(asdf:defsystem #:cl-blockcypher
		:description "A thin CLOS wrapper around blockcypher API"
		:author "Jim Hansson <jim.hansson@gmail.com>"
		:license  "Lisp Lesser General Public License (LLGPL)"
		:version "0.0.1"
		:depends-on ("drakma" "json-mop")
		:in-order-to ((asdf:test-op (asdf:test-op :cl-blockcypher/tests)))
		:pathname "src"
		:components ((:file "package")
								 (:file "cl-blockcypher")))

(asdf:defsystem #:cl-blockcypher/tests
		:description "A thin CLOS wrapper around blockcypher API tests"
		:author "Jim Hansson <jim.hansson@gmail.com>"
		:license  "Lisp Lesser General Public License (LLGPL)"
		:version "0.0.1"
		:depends-on ("cl-blockcypher"
								 "prove")
		:pathname "tests"
		:components ((:file "package")
								 (:file "tests"))
		:perform (asdf:test-op (op c) (funcall (intern #.(string :run) :prove) c)))
